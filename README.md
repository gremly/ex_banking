# ExBanking

This is an in memory system to store money for users as processes and allow them operations like
make deposits, withdraws, get current balances and execute transferences between users.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `ex_banking` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ex_banking, "~> 0.1.0"}
  ]
end
```

## Run tests

To run all tests:

```
mix test
```

## License

This work can be redistributed and/or modified under the terms of the MIT License.
