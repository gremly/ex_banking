defmodule ExBankingTest do
  use ExUnit.Case, async: true
  doctest ExBanking

  describe "user management" do
    test "creation succeeds" do
      assert :ok = ExBanking.create_user("Bruenor")
    end

    test "creation fails with wrong_arguments" do
      assert {:error, :wrong_arguments} = ExBanking.create_user(12345)
    end

    test "creation fails if the user already exists" do
      user = "Ulorin"
      assert :ok = ExBanking.create_user(user)
      assert {:error, :user_already_exists} = ExBanking.create_user(user)
    end
  end

  describe "user's money management" do
    setup do
      user = gen_username()
      :ok = ExBanking.create_user(user)
      {:ok, %{user: user}}
    end

    test "deposit in a currency for a user succeeds", %{user: user} do
      amount = 10.12
      assert {:ok, ^amount} = ExBanking.deposit(user, amount, "COP")
    end

    test "deposit in a currency increases user balance", %{user: user} do
      ExBanking.deposit(user, 10, "COP")
      assert {:ok, 20.12} = ExBanking.deposit(user, 10.12, "COP")
    end

    test "negative deposit in a currency for a user fails", %{user: user} do
      assert {:error, :wrong_arguments} = ExBanking.deposit(user, -10.12, "COP")
    end

    test "deposit in a currency for a non existent user fails" do
      assert {:error, :user_does_not_exist} = ExBanking.deposit("nouser", 1.02, "COP")
    end

    test "withdraw in a currency reduces user balance", %{user: user} do
      ExBanking.deposit(user, 10.12, "COP")
      assert {:ok, 0.12} = ExBanking.withdraw(user, 10, "COP")
    end

    test "withdraw for a new currency fails", %{user: user} do
      assert {:error, :not_enough_money} = ExBanking.withdraw(user, 10.12, "COP")
    end

    test "withdraw in a currency fails if there is no enough money", %{user: user} do
      ExBanking.deposit(user, 10, "COP")
      assert {:error, :not_enough_money} = ExBanking.withdraw(user, 10.12, "COP")
    end

    test "negative withdraw in a currency fails", %{user: user} do
      assert {:error, :wrong_arguments} = ExBanking.withdraw(user, -10.12, "COP")
    end

    test "withdraw in a currency for a non existent user fails" do
      assert {:error, :user_does_not_exist} = ExBanking.withdraw("nouser", 1.02, "COP")
    end

    test "get balance returns the current amount in the given currency", %{user: user} do
      amount = 10.12
      ExBanking.deposit(user, amount, "COP")
      assert {:ok, ^amount} = ExBanking.get_balance(user, "COP")
    end

    test "get balance for a non existent currency returns 0", %{user: user} do
      assert {:ok, 0.0} = ExBanking.get_balance(user, "COP")
    end

    test "get balance for a non existent user fails" do
      assert {:error, :user_does_not_exist} = ExBanking.get_balance("nouser", "COP")
    end
  end

  describe "user money transferences" do
    setup do
      sender = gen_username()
      :ok = ExBanking.create_user(sender)

      receiver = gen_username()
      :ok = ExBanking.create_user(receiver)
      {:ok, %{sender: sender, receiver: receiver}}
    end

    test "send money from one user to another succeed", context do
      ExBanking.deposit(context.sender, 10.12, "COP")
      assert {:ok, 4.71, 5.41} = ExBanking.send(context.sender, context.receiver, 5.41, "COP")
    end

    test "send money fails if sender has no money", context do
      assert {:error, :not_enough_money} =
               ExBanking.send(context.sender, context.receiver, 5.41, "COP")
    end

    test "send money from inexistent sender fails", context do
      assert {:error, :sender_does_not_exist} =
               ExBanking.send("nouser", context.receiver, 5.41, "COP")
    end

    test "send money to inexistent receiver fails", context do
      assert {:error, :receiver_does_not_exist} =
               ExBanking.send(context.sender, "nouser", 5.41, "COP")
    end
  end

  # Internal functions

  defp gen_username() do
    :crypto.strong_rand_bytes(16) |> Base.encode16()
  end
end
