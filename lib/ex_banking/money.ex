defmodule ExBanking.Money do
  alias Decimal

  @doc """
  Params verification and convertion before the actual
  decimal sum.
  """
  def add(a, b) when is_float(a) and is_float(b) do
    decimal_add(Float.to_string(a), Float.to_string(b))
  end

  def add(a, b) when is_float(a) do
    decimal_add(Float.to_string(a), b)
  end

  def add(a, b) when is_float(b) do
    decimal_add(a, Float.to_string(b))
  end

  def add(a, b) do
    decimal_add(a, b)
  end

  def decimal_add(a, b) do
    Decimal.add(a, b) |> Decimal.to_float()
  end

  @doc """
  Params verification and convertion before the actual
  decimal substraction.
  """
  def sub(a, b) when is_float(a) and is_float(b) do
    decimal_sub(Float.to_string(a), Float.to_string(b))
  end

  def sub(a, b) when is_float(a) do
    decimal_sub(Float.to_string(a), b)
  end

  def sub(a, b) when is_float(b) do
    decimal_sub(a, Float.to_string(b))
  end

  def sub(a, b) do
    decimal_sub(a, b)
  end

  def decimal_sub(a, b) do
    Decimal.sub(a, b) |> Decimal.to_float()
  end

  def negative?(number) when is_float(number) do
    number
    |> Float.to_string()
    |> Decimal.new()
    |> Decimal.negative?()
  end

  def negative?(number) do
    number
    |> Decimal.new()
    |> Decimal.negative?()
  end
end
