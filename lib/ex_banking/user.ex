defmodule ExBanking.User do
  @moduledoc """
  User which is identified by a name and maintains
  money in its state providing an API to manage it.
  """
  use GenServer

  alias ExBanking.Money

  @supervisor ExBanking.UserSupervisor

  @type banking_error ::
          {:error,
           :wrong_arguments
           | :user_already_exists
           | :user_does_not_exist
           | :not_enough_money
           | :sender_does_not_exist
           | :receiver_does_not_exist
           | :too_many_requests_to_user
           | :too_many_requests_to_sender
           | :too_many_requests_to_receiver}

  # Client API

  @spec create(user :: String.t()) :: :ok | banking_error
  def create(user) when is_binary(user) do
    perform_create(user)
  end

  def create(_), do: {:error, :wrong_arguments}

  @spec deposit(user :: String.t(), amount :: number, currency :: String.t()) ::
          {:ok, new_balance :: number} | banking_error
  def deposit(user, amount, currency)
      when is_binary(user) and amount > 0 and is_binary(currency) do

    perform(user, :call, {:deposit, amount, currency})
  end

  def deposit(_, _, _), do: {:error, :wrong_arguments}

  @spec withdraw(user :: String.t(), amount :: number, currency :: String.t()) ::
          {:ok, new_balance :: number} | banking_error
  def withdraw(user, amount, currency)
      when is_binary(user) and amount > 0 and is_binary(currency) do

    perform(user, :call, {:withdraw, amount, currency})
  end

  def withdraw(_, _, _), do: {:error, :wrong_arguments}

  @spec get_balance(user :: String.t(), currency :: String.t()) ::
          {:ok, balance :: number} | banking_error
  def get_balance(user, currency) when is_binary(user) and is_binary(currency) do
    perform(user, :call, {:get_balance, currency})
  end

  @spec send(
          sender :: String.t(),
          receiver :: String.t(),
          amount :: number,
          currency :: String.t()
        ) :: {:ok, sender_balance :: number, receiver_balance :: number} | banking_error
  def send(sender, receiver, amount, currency)
      when is_binary(sender) and amount > 0 and is_binary(receiver) and is_binary(currency) do

    with {:ok, from_pid, to_pid} <- get_users(sender, receiver),
         true <- accept_requests?(from_pid) do
      GenServer.call(from_pid, {:send, to_pid, amount, currency})
    else
      false -> {:error, :too_many_requests_to_sender}
      error -> error
    end
  end

  def send(_, _, _, _), do: {:error, :wrong_arguments}

  def start_link(user) do
    name = {:via, Registry, {Registry.User, user}}
    GenServer.start_link(__MODULE__, [], name: name)
  end

  # GenServer callbacks

  def init(_) do
    {:ok, %{}}
  end

  def handle_call({:deposit, amount, currency}, _, state) do
    currency_atom = currency_name(currency)
    balance = perform_deposit(amount, state[currency_atom])
    {:reply, balance, update_state(balance, currency_atom, state)}
  end

  def handle_call({:withdraw, amount, currency}, _, state) do
    currency_atom = currency_name(currency)
    balance = perform_withdraw(amount, state[currency_atom])
    {:reply, balance, update_state(balance, currency_atom, state)}
  end

  def handle_call({:get_balance, currency}, _, state) do
    currency_atom = currency_name(currency)
    {:reply, perform_get_balance(state[currency_atom]), state}
  end

  def handle_call({:send, receiver, amount, currency}, _, state) do
    currency_atom = currency_name(currency)

    response =
      with {:ok, sender_balance} <- perform_withdraw(amount, state[currency_atom]),
           true <- accept_requests?(receiver),
           {:ok, receiver_balance} <- GenServer.call(receiver, {:deposit, amount, currency}) do
        {:ok, sender_balance, receiver_balance}
      else
        false -> {:error, :too_many_requests_to_receiver}
        error -> error
      end

    {:reply, response, update_state({elem(response, 0), elem(response, 1)}, currency_atom, state)}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  # Internal functions

  defp currency_name(currency), do: String.to_atom(currency)

  def perform(user, type, message) do
    with {:ok, pid} <- get_user_pid(user),
         true <- accept_requests?(pid) do
      apply(GenServer, type, [pid, message])
    else
      false -> {:error, :too_many_requests_to_user}
      _ -> {:error, :user_does_not_exist}
    end
  end

  defp get_user_pid(user) do
    case Registry.lookup(Registry.User, user) do
      [] -> {:error, :user_does_not_exist}
      [{pid, _}] -> {:ok, pid}
    end
  end

  defp perform_create(user) do
    case DynamicSupervisor.start_child(@supervisor, {__MODULE__, user}) do
      {:ok, _pid} -> :ok
      {:error, {:already_started, _}} -> {:error, :user_already_exists}
    end
  end

  defp perform_deposit(amount, nil), do: {:ok, amount}
  defp perform_deposit(amount, current), do: {:ok, Money.add(amount, current)}

  defp perform_withdraw(_, nil), do: {:error, :not_enough_money}

  defp perform_withdraw(amount, current) do
    result = Money.sub(current, amount)

    if Money.negative?(result) do
      {:error, :not_enough_money}
    else
      {:ok, result}
    end
  end

  defp perform_get_balance(nil), do: {:ok, 0.0}
  defp perform_get_balance(balance), do: {:ok, balance}

  defp update_state({:ok, balance}, currency, state), do: Map.put(state, currency, balance)
  defp update_state({:error, _}, _, state), do: state

  defp get_users(sender, receiver) do
    sender_pid = get_user_pid(sender)
    receiver_pid = get_user_pid(receiver)

    case {sender_pid, receiver_pid} do
      {{:error, _}, _} -> {:error, :sender_does_not_exist}
      {_, {:error, _}} -> {:error, :receiver_does_not_exist}
      {{:ok, sender}, {:ok, receiver}} -> {:ok, sender, receiver}
    end
  end

  defp accept_requests?(pid) do
    {_, msg_len} = Process.info(pid, :message_queue_len)
    msg_len < 10
  end
end
