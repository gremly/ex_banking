defmodule ExBanking do
  @moduledoc """
  Documentation for ExBanking.
  """

  alias ExBanking.User

  @type banking_error ::
          {:error,
           :wrong_arguments
           | :user_already_exists
           | :user_does_not_exist
           | :not_enough_money
           | :sender_does_not_exist
           | :receiver_does_not_exist
           | :too_many_requests_to_user
           | :too_many_requests_to_sender
           | :too_many_requests_to_receiver}

  @doc """
  Create a new user in the system
  """
  @spec create_user(user :: String.t()) :: :ok | banking_error
  defdelegate create_user(user), to: User, as: :create

  @doc """
  Deposit the given amount in a currency to an user.
  """
  @spec deposit(user :: String.t(), amount :: number, currency :: String.t()) ::
          {:ok, new_balance :: number} | banking_error
  defdelegate deposit(user, amount, currency), to: User

  @doc """
  Withdraw the given amount in a currency from the user money If there is no enough
  money an error is returned.
  """
  @spec withdraw(user :: String.t(), amount :: number, currency :: String.t()) ::
          {:ok, new_balance :: number} | banking_error
  defdelegate withdraw(user, amount, currency), to: User

  @doc """
  Returns the curren balance for an user in the given currency.
  """
  @spec get_balance(user :: String.t(), currency :: String.t()) ::
          {:ok, balance :: number} | banking_error
  defdelegate get_balance(user, currency), to: User

  @doc """
  Executes a transference from one user to another in the given
  currency.
  """
  @spec send(
          from_user :: String.t(),
          to_user :: String.t(),
          amount :: number,
          currency :: String.t()
        ) :: {:ok, from_user_balance :: number, to_user_balance :: number} | banking_error
  defdelegate send(from_user, to_user, amount, currency), to: User
end
